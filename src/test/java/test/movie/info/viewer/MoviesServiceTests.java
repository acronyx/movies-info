package test.movie.info.viewer;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import test.movie.info.viewer.access.MoviesDataSource;
import test.movie.info.viewer.access.MoviesDataSourceException;
import test.movie.info.viewer.service.CalculationProgressCallback;
import test.movie.info.viewer.service.MoviesService;
import test.movie.info.viewer.shared.Genre;
import test.movie.info.viewer.shared.Movie;
import test.movie.info.viewer.shared.MoviesInfo;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MoviesServiceTests {

    @Test
    void calculationWithoutCancellationTokenDoesNotThrow() {
        MoviesService moviesService = singleThreadMoviesService(staticMoviesDataSource());

        Assertions.assertDoesNotThrow(
                () -> moviesService.calculateAverageRatingUsingGenreId(1, (_1, _2) -> {
                }, null));
    }

    @Test
    void calculationWithoutProgressCallbackDoesNotThrow() {
        MoviesService moviesService = singleThreadMoviesService(staticMoviesDataSource());

        Assertions.assertDoesNotThrow(
                () -> moviesService.calculateAverageRatingUsingGenreId(1, null, new AtomicBoolean(false)));
    }


    @Test
    void calculationWithProgressCallbackWorks() {
        MoviesService moviesService = singleThreadMoviesService(staticMoviesDataSource());

        AtomicInteger calls = new AtomicInteger();
        CalculationProgressCallback callback = new CalculationProgressCallback() {
            private int lastValue = 0;

            @Override
            public void accept(Integer value, Integer max) {
                synchronized (this) {
                    Assertions.assertEquals(lastValue + 1, value);
                    lastValue = value;
                }
                calls.addAndGet(1);
            }
        };

        moviesService.calculateAverageRatingUsingGenreId(1, callback, null);

        Assertions.assertEquals(9, calls.get());
    }

    @SneakyThrows
    @Test
    void calculationCancellationWorks() {
        MoviesDataSource moviesDataSource = mock(MoviesDataSource.class);

        CountDownLatch responseStartLatch = new CountDownLatch(1);
        when(moviesDataSource.getMoviesInfo(anyInt(), anyInt())).then(invocation -> {
            responseStartLatch.await();
            return MoviesInfo.builder()
                    .page(invocation.getArgument(0))
                    .totalPages(3) // 2, сервер выдает число страниц больше на 1
                    .results(Collections.emptyList())
                    .totalResults(0)
                    .build();
        });

        MoviesService moviesService = singleThreadMoviesService(moviesDataSource);

        AtomicBoolean cancellationToken = new AtomicBoolean();

        CountDownLatch cancellationLatch = new CountDownLatch(1);

        new Thread(() -> {
            try {
                cancellationLatch.await();
                cancellationToken.set(true);
                responseStartLatch.countDown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        Assertions.assertThrows(
                CancellationException.class,
                () -> {
                    cancellationLatch.countDown();
                    moviesService.calculateAverageRatingUsingGenreId(1, null, cancellationToken);
                });
    }

    @Test
    void calculationTakesIntoAccountSelectedGenreOnly() {
        MoviesService moviesService = singleThreadMoviesService(staticMoviesDataSource());

        Assertions.assertEquals(
                0.0,
                moviesService.calculateAverageRatingUsingGenreId(0, null, null));
    }

    @Test
    void calculationTakesIntoAccountAllMovies() {
        MoviesService moviesService = singleThreadMoviesService(staticMoviesDataSource());

        Assertions.assertEquals(
                5.5,
                moviesService.calculateAverageRatingUsingGenreId(1, null, null));
    }

    @Test
    void calculationTaskDoesNotReturnResultIfCatchesException() {
        MoviesService moviesService = singleThreadMoviesService(crashingMoviesDataSource());

        Assertions.assertThrows(
                MoviesDataSourceException.class,
                () -> moviesService.calculateAverageRatingUsingGenreId(1, null, null)
        );
    }

    private MoviesService singleThreadMoviesService(MoviesDataSource moviesDataSource) {
        return new MoviesService(
                Executors.newFixedThreadPool(1),
                moviesDataSource);
    }

    /**
     * Возвращает источник данных, включающий в себя:
     * - 10 жанров с id от 1 до 10 и такими же именами
     * - 10 фильмов 1-го жанра на одной странице с последовательным рейтингом от 1 до 10 и средним 5.5
     */
    private MoviesDataSource staticMoviesDataSource() {
        MoviesDataSource moviesDataSource = mock(MoviesDataSource.class);
        when(moviesDataSource.getGenres()).then(
                invocation -> Stream.iterate(1, i -> i + 1)
                        .limit(10)
                        .map(i -> new Genre(i, Integer.toString(i)))
                        .collect(Collectors.toList()));
        when(moviesDataSource.getMoviesInfo(anyInt(), anyInt())).then(
                invocation -> {
                    final int pages = 9;
                    final int page = invocation.getArgument(0);
                    final List<Movie> movies = Stream.iterate(1, i -> i + 1).limit(10)
                            .map(i -> Movie.builder()
                                    .voteAverage(i)
                                    .genreIds(Collections.singletonList(1))
                                    .build())
                            .collect(Collectors.toList());

                    if (page > pages || page < 1)
                        throw new MoviesDataSourceException("pages");

                    return MoviesInfo.builder()
                            .page(page)
                            .totalPages(pages + 1) // сервер по каким-то причинам выдает на 1 страницу больше, чем есть
                            .results(movies)
                            .totalResults(pages * movies.size())
                            .build();
                });
        return moviesDataSource;
    }

    private MoviesDataSource crashingMoviesDataSource() {
        MoviesDataSource moviesDataSource = mock(MoviesDataSource.class);
        when(moviesDataSource.getGenres()).thenThrow(new MoviesDataSourceException("test"));
        when(moviesDataSource.getMoviesInfo(anyInt(), anyInt())).thenThrow(new MoviesDataSourceException("test"));
        return moviesDataSource;
    }
}

package test.movie.info.viewer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import test.movie.info.viewer.config.GuiConfig;
import test.movie.info.viewer.shared.GenresInfo;
import test.movie.info.viewer.shared.MoviesInfo;

@SpringBootTest
public class JacksonDataBindingTests {

    @MockBean
    private GuiConfig guiConfig;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void genresBindingDoesNotThrow() {
        String genresResponse = "{\"genres\":[{\"id\":10752,\"name\":\"War\"},{\"id\":10402,\"name\":\"Music\"},{\"id\":99,\"name\":\"Documentary\"},{\"id\":35,\"name\":\"Comedy\"},{\"id\":36,\"name\":\"History\"},{\"id\":37,\"name\":\"Western\"},{\"id\":12,\"name\":\"Adventure\"},{\"id\":878,\"name\":\"Science Fiction\"},{\"id\":14,\"name\":\"Fantasy\"},{\"id\":9648,\"name\":\"Mystery\"},{\"id\":80,\"name\":\"Crime\"},{\"id\":16,\"name\":\"Animation\"},{\"id\":10770,\"name\":\"TV Movie\"},{\"id\":18,\"name\":\"Drama\"},{\"id\":53,\"name\":\"Thriller\"},{\"id\":27,\"name\":\"Horror\"},{\"id\":28,\"name\":\"Action\"},{\"id\":10749,\"name\":\"Romance\"},{\"id\":10751,\"name\":\"Family\"}]}";
        Assertions.assertDoesNotThrow(() -> objectMapper.readValue(genresResponse, GenresInfo.class));
    }


    @Test
    void moviesBindingDoesNotThrow() {
        String moviesResponse = "{\"page\":1957,\"total_results\":19568,\"total_pages\":1958,\"results\":[{\"id\":556792,\"vote_count\":0,\"video\":false,\"vote_average\":0.0,\"title\":\"Mommy Heist\",\"popularity\":2.268,\"original_language\":\"fr\",\"original_title\":\"Mommy Heist\",\"genre_ids\":[],\"adult\":false,\"overview\":\"\",\"release_date\":[2018,10,20]},{\"id\":556793,\"vote_count\":0,\"video\":false,\"vote_average\":0.0,\"title\":\"Même pas mal, petit animal\",\"popularity\":2.268,\"original_language\":\"fr\",\"original_title\":\"Même pas mal, petit animal\",\"genre_ids\":[],\"adult\":false,\"overview\":\"\",\"release_date\":[2018,10,20]},{\"id\":556797,\"vote_count\":0,\"video\":false,\"vote_average\":0.0,\"title\":\"Der Anti-Trump - Senator John McCain\",\"popularity\":2.744,\"original_language\":\"de\",\"original_title\":\"Der Anti-Trump - Senator John McCain\",\"genre_ids\":[99],\"adult\":false,\"overview\":\"\",\"release_date\":[2018,10,25]},{\"id\":556802,\"vote_count\":2,\"video\":false,\"vote_average\":7.0,\"title\":\"Santo cachón\",\"popularity\":2.716,\"original_language\":\"es\",\"original_title\":\"Santo cachón\",\"genre_ids\":[35],\"adult\":false,\"overview\":\"\",\"release_date\":[2018,10,26]},{\"id\":556835,\"vote_count\":0,\"video\":false,\"vote_average\":0.0,\"title\":\"Lucid Dreams\",\"popularity\":2.744,\"original_language\":\"zh\",\"original_title\":\"Lucid Dreams\",\"genre_ids\":[53,27,18,35,10402],\"adult\":false,\"overview\":\"\",\"release_date\":[2018,10,25]},{\"id\":556855,\"vote_count\":0,\"video\":false,\"vote_average\":0.0,\"title\":\"Cuando respiro en tu boca. La creación de Peces\",\"popularity\":2.744,\"original_language\":\"es\",\"original_title\":\"Cuando respiro en tu boca. La creación de Peces\",\"genre_ids\":[99],\"adult\":false,\"overview\":\"Santiago de Chile, 1994: financed by mysterious textile entrepreneurs, the rock band Lucybell enters the Sonus Studios to record their first album, by the hand of the famous Argentine producer Mario Breuer. The budget is enough for only eight days.  Santiago de Chile, 2017: after being disappeared for twenty-three years, the videotapes with the chronicle of those eight days come to light. This was the creation of Peces, one of the most important Chilean records of the '90s.\",\"release_date\":[2018,10,25]},{\"id\":556900,\"vote_count\":0,\"video\":false,\"vote_average\":0.0,\"title\":\"RIPTIDE: Black Water 2018\",\"popularity\":2.744,\"original_language\":\"en\",\"original_title\":\"RIPTIDE: Black Water 2018\",\"genre_ids\":[],\"adult\":false,\"overview\":\"CONFIRMED:  - CHUCK MAMBO defends the Brighton Championship!  - TK COOPER vs VIPER vs MILLIE MCKENZIE vs CANDY FLOSS vs FLASH MORGAN WEBSTER  (Winner receives a Brighton Championship opportunity at the RIPTIDE Rumble 2019)  - CHRIS BROOKES vs DAVID STARR  - 'SPEEDBALL' MIKE BAILEY vs CARA NOIR  - MONEY VS EVERYBODY vs THE OJMO & TEAM WHITE WOLF  - LORD GIDEON GREY vs JORDON BREAKS  - THE AUSSIE OPEN CHALLENGE  - KRIS WOLF vs \\\"KEYBOARD WARRIOR\\\" KURTIS CHAPMAN\",\"release_date\":[2018,11,2]},{\"id\":556907,\"vote_count\":2,\"video\":false,\"vote_average\":8.0,\"title\":\"Baazaar\",\"popularity\":2.744,\"original_language\":\"hi\",\"original_title\":\"Baazaar\",\"genre_ids\":[80,18,53],\"adult\":false,\"overview\":\"Baazaar is a 2018 film starring Saif Ali Khan, Chitrangada Singh and Radhika Apte. With a plot revolving around stock-trading, the film, according to director Gauravv K. Chawla's is about making it in the big bad world of Mumbai.\",\"release_date\":[2018,10,26]}]}";
        Assertions.assertDoesNotThrow(() -> objectMapper.readValue(moviesResponse, MoviesInfo.class));
    }
}

package test.movie.info.viewer.access;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import test.movie.info.viewer.shared.Genre;
import test.movie.info.viewer.shared.GenresInfo;
import test.movie.info.viewer.shared.MoviesInfo;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

@Component
@Slf4j
@RequiredArgsConstructor
public class RemoteMoviesDataSource implements MoviesDataSource {

    private final OkHttpClient httpClient = new OkHttpClient();

    private final ObjectMapper objectMapper;

    @Value("${api.key}")
    private String apiKey;

    @Override
    public List<Genre> getGenres() {
        try {
            log.debug("Get genres");

            ResponseBody responseBody = requireNonNullResponseBody(httpClient.newCall(new Request.Builder()
                    .url("https://easy.test-assignment-a.loyaltyplant.net/3/genre/movie/list?api_key=" + apiKey)
                    .get()
                    .build())
                    .execute());

            return objectMapper.readValue(responseBody.string(), GenresInfo.class).getGenres();
        } catch (IOException e) {
            log.error("Genres extraction is failed", e);
            throw new MoviesDataSourceException("Genres extraction is failed", e);
        }
    }

    @Override
    public MoviesInfo getMoviesInfo(int page, int genreId) {
        try {
            log.debug("Get movies info, page={}, genre={}", page, genreId);

            ResponseBody responseBody = requireNonNullResponseBody(httpClient.newCall(new Request.Builder()
                    .url("https://easy.test-assignment-a.loyaltyplant.net/3/discover/movie?api_key=" + genreId + "&page=" + page)
                    .get()
                    .build())
                    .execute());

            return objectMapper.readValue(responseBody.string(), MoviesInfo.class);
        } catch (IOException e) {
            log.error("Genres extraction is failed", e);
            throw new MoviesDataSourceException("Genres extraction is failed, genreId=" + genreId + ", page=" + page, e);
        }
    }

    private static ResponseBody requireNonNullResponseBody(Response response) {
        ResponseBody responseBody = response.body();
        if (Objects.isNull(responseBody))
            throw new MoviesDataSourceException("Empty response");
        return responseBody;
    }
}

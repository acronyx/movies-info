package test.movie.info.viewer.access;

import test.movie.info.viewer.shared.Genre;
import test.movie.info.viewer.shared.MoviesInfo;

import java.util.List;

public interface MoviesDataSource {

    List<Genre> getGenres();

    MoviesInfo getMoviesInfo(int page, int genreId);
}

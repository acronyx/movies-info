package test.movie.info.viewer.access;

public class MoviesDataSourceException extends RuntimeException {

    public MoviesDataSourceException(String message) {
        super(message);
    }

    public MoviesDataSourceException(String message, Throwable cause) {
        super(message, cause);
    }
}

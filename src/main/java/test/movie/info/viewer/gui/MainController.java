package test.movie.info.viewer.gui;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import test.movie.info.viewer.gui.model.GenreViewProxy;
import test.movie.info.viewer.service.MoviesService;
import test.movie.info.viewer.shared.Genre;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

@RequiredArgsConstructor
@Slf4j
public class MainController {

    private JFrame frame;
    private final GenreViewProxy[] defaultSingleGenre = {new GenreViewProxy(new Genre(-1, "No loaded genres"))};
    private final DefaultComboBoxModel<GenreViewProxy> genreListModel = new DefaultComboBoxModel<>(defaultSingleGenre);
    private JButton stopAverageRatingCalculationButton;
    private JProgressBar averageRatingCalculationProgressBar;
    private JLabel averageRatingCalculationResultLabel;
    private final AtomicBoolean calculationCancellationToken = new AtomicBoolean(false);

    private final ExecutorService executorService;
    private final MoviesService moviesService;

    {
        createUI();
    }

    private void createUI() {
        createForm();
        createGenrePanel();
    }

    private void createForm() {
        frame = new JFrame();
        frame.setTitle("Movie Info Viewer");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(600, 200);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new BorderLayout());
    }

    private void createGenrePanel() {
        JPanel genrePanel = new JPanel(new FlowLayout());

        JComboBox<GenreViewProxy> genreList = new JComboBox<>(genreListModel);
        genrePanel.add(genreList, BorderLayout.PAGE_START);

        JButton loadGenresButton = new JButton("Load genres");
        loadGenresButton.addActionListener(createLoadGenresButtonListener(loadGenresButton));
        genrePanel.add(loadGenresButton);

        JButton calculateAverageRatingButton = new JButton("Calculate votes average");
        calculateAverageRatingButton.addActionListener(createCalculateAverageRatingButtonListener(calculateAverageRatingButton));
        genrePanel.add(calculateAverageRatingButton);

        averageRatingCalculationResultLabel = new JLabel();
        averageRatingCalculationResultLabel.setVisible(false);
        genrePanel.add(averageRatingCalculationResultLabel);

        stopAverageRatingCalculationButton = new JButton("Stop average rating calculation");
        stopAverageRatingCalculationButton.addActionListener(createStopAverageRatingCalculationButton(stopAverageRatingCalculationButton));
        stopAverageRatingCalculationButton.setVisible(false);
        genrePanel.add(stopAverageRatingCalculationButton);

        averageRatingCalculationProgressBar = new JProgressBar(0, 100);
        averageRatingCalculationProgressBar.setVisible(false);
        genrePanel.add(averageRatingCalculationProgressBar);

        frame.add(genrePanel);
    }

    private ActionListener createStopAverageRatingCalculationButton(JButton stopAverageRatingCalculationButton) {
        return event -> {
            stopAverageRatingCalculationButton.setEnabled(false);
            calculationCancellationToken.set(true);
        };
    }

    private ActionListener createLoadGenresButtonListener(JButton loadGenresButton) {
        return event -> {
            loadGenresButton.setText("Loading...");
            loadGenresButton.setEnabled(false);

            executorService.execute(() -> {
                try {
                    List<Genre> genres = moviesService.findAllGenres();

                    SwingUtilities.invokeLater(() -> {
                        genreListModel.removeAllElements();
                        if (genres.isEmpty()) {
                            genreListModel.addElement(defaultSingleGenre[0]);
                        } else {
                            genres.stream()
                                    .map(GenreViewProxy::new)
                                    .forEach(genreListModel::addElement);
                        }
                    });
                } catch (Exception e) {
                    log.error("Genres loading  is  failed", e);
                    SwingUtilities.invokeLater(() ->
                            JOptionPane.showMessageDialog(frame, "Genres loading is failed: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE));
                } finally {
                    SwingUtilities.invokeLater(() -> {
                        loadGenresButton.setText("Load genres");
                        loadGenresButton.setEnabled(true);
                    });
                }
            });
        };
    }

    private ActionListener createCalculateAverageRatingButtonListener(JButton calculateAverageRatingButton) {
        return event -> {
            GenreViewProxy genreViewProxy = (GenreViewProxy) genreListModel.getSelectedItem();

            if (Objects.isNull(genreViewProxy)) {
                showRequirementToLoadGenres();
                return;
            }

            Genre genre = genreViewProxy.getGenre();

            if (Objects.equals(genre, defaultSingleGenre[0].getGenre())) {
                showRequirementToLoadGenres();
                return;
            }

            int genreId = genre.getId();

            calculateAverageRatingButton.setEnabled(false);
            stopAverageRatingCalculationButton.setVisible(true);
            stopAverageRatingCalculationButton.setEnabled(true);
            averageRatingCalculationProgressBar.setVisible(true);
            calculationCancellationToken.set(false);

            executorService.execute(() -> {
                try {
                    setAverageRatingCalculationResultAndShowNotification(
                            moviesService.calculateAverageRatingUsingGenreId(genreId, this::setCalculationProgress, calculationCancellationToken));
                    averageRatingCalculationResultLabel.setVisible(true);
                } catch (CancellationException e) {
                    log.info("Rating calculation was cancelled");
                } catch (Exception e) {
                    log.error("Rating calculation is failed", e);
                    SwingUtilities.invokeLater(() ->
                            JOptionPane.showMessageDialog(frame, "Rating calculation is failed: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE));
                } finally {
                    SwingUtilities.invokeLater(() -> {
                        calculateAverageRatingButton.setEnabled(true);
                        stopAverageRatingCalculationButton.setVisible(false);
                    });
                }
            });
        };
    }

    public void show() {
        SwingUtilities.invokeLater(() -> frame.setVisible(true));
    }

    private void showRequirementToLoadGenres() {
        JOptionPane.showMessageDialog(frame, "First, load genres", "Warning", JOptionPane.WARNING_MESSAGE);
    }

    private void showCalculationCompletionNotification() {
        JOptionPane.showMessageDialog(frame, "Calculation task is done", "Information", JOptionPane.INFORMATION_MESSAGE);
    }

    private void setCalculationProgress(int value, int max) {
        SwingUtilities.invokeLater(() -> {
            averageRatingCalculationProgressBar.setValue(value);
            averageRatingCalculationProgressBar.setMaximum(max);
        });
    }

    private void setAverageRatingCalculationResultAndShowNotification(double value) {
        SwingUtilities.invokeLater(() -> {
            averageRatingCalculationResultLabel.setText("Avg: " + new DecimalFormat("0.000").format(value));
            showCalculationCompletionNotification();
        });
    }
}

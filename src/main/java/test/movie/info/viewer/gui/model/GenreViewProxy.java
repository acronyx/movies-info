package test.movie.info.viewer.gui.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import test.movie.info.viewer.shared.Genre;

@Getter
@AllArgsConstructor
public class GenreViewProxy {

    private final Genre genre;

    @Override
    public String toString() {
        return genre.getName();
    }
}

package test.movie.info.viewer;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import test.movie.info.viewer.gui.MainController;

@AllArgsConstructor(onConstructor = @__(@Autowired))
public class GuiRunner implements ApplicationRunner {

    private final MainController mainController;

    @Override
    public void run(ApplicationArguments args) {
        mainController.show();
    }
}

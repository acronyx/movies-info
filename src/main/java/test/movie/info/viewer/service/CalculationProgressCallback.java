package test.movie.info.viewer.service;

import java.util.function.BiConsumer;

@FunctionalInterface
public interface CalculationProgressCallback extends BiConsumer<Integer, Integer> {

    @Override
    void accept(Integer value, Integer max);
}

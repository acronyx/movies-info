package test.movie.info.viewer.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import test.movie.info.viewer.access.MoviesDataSource;
import test.movie.info.viewer.shared.Genre;
import test.movie.info.viewer.shared.Movie;
import test.movie.info.viewer.shared.MoviesInfo;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class MoviesService {

    private final ExecutorService executorService;

    private final MoviesDataSource moviesDataSource;

    public List<Genre> findAllGenres() {
        return moviesDataSource.getGenres();
    }

    @SneakyThrows
    public double calculateAverageRatingUsingGenreId(int genreId,
                                                     CalculationProgressCallback calculationProgressCallback,
                                                     AtomicBoolean cancellationToken) {
        log.debug("Calculate average rating, genreId={}, calculationProgressCallback={}", genreId, calculationProgressCallback);

        List<Double> ratings = Collections.synchronizedList(new LinkedList<>());

        MoviesInfo firstInfoPage = moviesDataSource.getMoviesInfo(1, genreId);
        ratings.addAll(extractRatingsFromMoviesInfo(genreId, firstInfoPage));

        if (!Objects.isNull(calculationProgressCallback))
            calculationProgressCallback.accept(1, firstInfoPage.getTotalPages() - 1);

        if (firstInfoPage.getTotalPages() > 2) {
            AtomicInteger pagesProgress = new AtomicInteger(1);

            List<Future<Void>> futures = executorService.invokeAll(
                    Stream.iterate(2, page -> page + 1)
                            .limit(firstInfoPage.getTotalPages() - 2)
                            .map(page -> (Callable<Void>) () -> {
                                if (Objects.isNull(cancellationToken) || !cancellationToken.get()) {
                                    try {
                                        MoviesInfo info = moviesDataSource.getMoviesInfo(page, genreId);
                                        ratings.addAll(extractRatingsFromMoviesInfo(genreId, info));
                                        return null;
                                    } catch (Exception e) {
                                        log.error("Movie data retrieving is failed [page=" + page + "]", e);
                                        throw e;
                                    } finally {
                                        if (!Objects.isNull(calculationProgressCallback))
                                            calculationProgressCallback.accept(pagesProgress.incrementAndGet(), firstInfoPage.getTotalPages() - 1);
                                    }
                                } else {
                                    return null;
                                }
                            })
                            .collect(Collectors.toList()));

            try {
                for (Future<Void> future : futures) future.get();

                if (!Objects.isNull(cancellationToken) && cancellationToken.get())
                    throw new CancellationException("Task was cancelled");

            } catch (InterruptedException | ExecutionException e) {
                log.error("Tasks execution is failed", e);
                futures.forEach(f -> f.cancel(false));
                throw e;
            }
        }

        return ratings.stream()
                .mapToDouble(d -> d)
                .average()
                .orElse(0);
    }

    private List<Double> extractRatingsFromMoviesInfo(int genreId, MoviesInfo info) {
        return info.getResults().stream()
                .filter(movie -> movie.getGenreIds().contains(genreId))
                .map(Movie::getVoteAverage)
                .collect(Collectors.toList());
    }
}

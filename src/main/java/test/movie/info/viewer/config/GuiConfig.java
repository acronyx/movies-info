package test.movie.info.viewer.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import test.movie.info.viewer.GuiRunner;
import test.movie.info.viewer.gui.MainController;
import test.movie.info.viewer.service.MoviesService;

import java.util.concurrent.ExecutorService;

@Configuration
public class GuiConfig {

    @Bean
    public MainController mainView(ExecutorService executorService,
                                   MoviesService moviesService) {
        return new MainController(executorService, moviesService);
    }

    @Bean
    public GuiRunner guiRunner(MainController mainController) {
        return new GuiRunner(mainController);
    }
}

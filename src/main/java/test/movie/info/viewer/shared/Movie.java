package test.movie.info.viewer.shared;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Movie {

    private int id;
    private int voteCount;
    private boolean video;
    private double voteAverage;
    private String title;
    private double popularity;
    private String originalLanguage;
    private String originalTitle;
    private List<Integer> genreIds;
    private boolean adult;
    private String overview;
    private LocalDate releaseDate;
}

package test.movie.info.viewer.shared;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MoviesInfo {

    private int page;
    private int totalResults;
    private int totalPages;
    private List<Movie> results;
}
